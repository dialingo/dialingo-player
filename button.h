#ifndef BUTTON_H
#define BUTTON_H

#include <QAbstractButton>

class Button : public QAbstractButton
{
    Q_OBJECT
public:
    explicit Button(QWidget *parent = 0);
    void paintEvent(QPaintEvent *e);

    QSize sizeHint () const;
    void setBackground(const QPixmap &up, const QPixmap &down);
    void setToggleDown(const QPixmap toggleDown);
    void mousePressEvent ( QMouseEvent * event );
    void mouseReleaseEvent ( QMouseEvent * event );

private:
    QPixmap m_up;
    QPixmap m_down;
    bool m_isPressed;
    bool m_buttonDown;
};

#endif // BUTTON_H
