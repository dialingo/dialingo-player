#include <QPainter>
#include <QDebug>

#include "button.h"


Button::Button(QWidget *parent) :
    QAbstractButton(parent)
{
    m_isPressed = false;
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_buttonDown = false;
}

void Button::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.drawPixmap(0,0, m_isPressed? m_down: m_up);
    if(isChecked())
    {
        icon().paint(&painter, QRect(0, 0, m_up.size().width(), m_up.size().height()   ), Qt::AlignCenter, QIcon::Normal, QIcon::On);
        //qDebug() << "painted pause button";
        //paint ( QPainter * painter, const QRect & rect, Qt::Alignment alignment = Qt::AlignCenter, Mode mode = Normal, State state = Off ) const
    }else{
        icon().paint(&painter, QRect(0, 0, m_up.size().width(), m_up.size().height()   ));
    }
}

QSize Button::sizeHint() const
{
    //qDebug() << m_up.size();
    return m_up.size();
}

void Button::setBackground(const QPixmap &up, const QPixmap &down)
{
    m_up = up;
    m_down = down;
    update();
    updateGeometry();

}

void Button::mousePressEvent(QMouseEvent *event)
{
    m_isPressed= true;
    update();
    QAbstractButton::mousePressEvent(event);
    //emit clicked();
}

void Button::mouseReleaseEvent(QMouseEvent *event)
{
    m_isPressed=false;
    update();
    QAbstractButton::mouseReleaseEvent(event);
    m_buttonDown = !m_buttonDown;
}



