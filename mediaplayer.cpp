#include <QEventLoop>
#include <QTimer>

#include "mediaplayer.h"

MediaPlayer::MediaPlayer(QVideoWidget *videoWidget)
{
    m_mediaPlayer.setVideoOutput(videoWidget);
}

void MediaPlayer::loadBlocking(const QString &file)
{
    m_mediaPlayer.setMedia(QUrl::fromLocalFile(file));
    if(!m_mediaPlayer.isSeekable())
    {
        QEventLoop loop;
        QTimer timer;
        timer.setSingleShot(true);
        timer.setInterval(2000);
        QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit );
        QObject::connect(&m_mediaPlayer, &QMediaPlayer::seekableChanged, &loop, &QEventLoop::quit);

        timer.start();
        loop.exec();
    }
}

void MediaPlayer::setPosition(qint64 pos)
{
    return m_mediaPlayer.setPosition(pos);
}

qint64 MediaPlayer::position()
{
    return m_mediaPlayer.position();
}

void MediaPlayer::play()
{
    m_mediaPlayer.play();
}

void MediaPlayer::pause()
{
    m_mediaPlayer.pause();
}

void MediaPlayer::stop()
{
    m_mediaPlayer.stop();
}

MediaplayerState MediaPlayer::state()
{
    MediaplayerState state;
    switch(m_mediaPlayer.state())
    {
    case QMediaPlayer::StoppedState:
        state = MediaplayerState::Stopped;
        break;
    case QMediaPlayer::PlayingState:
        state = MediaplayerState::Playing;
        break;
    case QMediaPlayer::PausedState:
        state =  MediaplayerState::Stopped;
        break;
    }
    return state;
}


