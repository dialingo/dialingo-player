#include <QTextBlock>
#include <QDebug>

#include "texteditext.h"



TextEditExt::TextEditExt()
{
    setContextMenuPolicy(Qt::NoContextMenu);
}

//----------------------------------------------------------------
void TextEditExt::setRanges(const QList<TextRange> &range)
{
    m_ranges =range;
}

//----------------------------------------------------------------
void TextEditExt::mousePressEvent(QMouseEvent *event)
{
    QTextCursor textCursor = cursorForPosition(event->pos());
    int block = textCursor.blockNumber();
    int offset = textCursor.positionInBlock();
    int record = recordFromCoordinate(block, offset);
    if(event->button() & Qt::RightButton)
    {
        QTextCursor middle = textCursor;
        ///@TODO: improvement needed to select Al-Qaeda instead of Al and Qaeda.
        middle.movePosition(QTextCursor::StartOfWord);
        middle.movePosition(QTextCursor::EndOfWord, QTextCursor::KeepAnchor);

        const QPoint popupPosition = mapToGlobal(event->pos());
        emit translationRequested(popupPosition, middle.selectedText() );
    }
    else
    {
        if(record >= 0)
        {
            emit recordChanged(record);
        }
    }
}

//----------------------------------------------------------------
void TextEditExt::mouseReleaseEvent(QMouseEvent *e)
{
    if(e->button() & Qt::RightButton )
    {
        emit hideTranslation();
    }
}

//----------------------------------------------------------------
void TextEditExt::mouseDoubleClickEvent(QMouseEvent *e)
{
    // ignore mouse double Click
}

//----------------------------------------------------------------
int TextEditExt::recordFromCoordinate(int block, int offset) const
{
    block++;
    for(int i= 0; i< m_ranges.size(); i++)
    {
        const TextRange &range = m_ranges.at(i);
           if(range.fromBlock <= block && range.toBlock >= block
                && range.fromOffset <= offset && range.toOffset >= offset )
        {
            return i;
        }
        if (range.fromBlock > block)
        {
            return -1; // already gone too far
        }
    }
    return -1;
}

//----------------------------------------------------------------
//! returns the index of the current selection.
int TextEditExt::selection() const
{
    return m_selection;
}

//----------------------------------------------------------------
bool TextEditExt::setSelection(int selection, QString *text)
{
    if(selection >=0 && selection < m_ranges.size() )
    {
        m_selection = selection;
        setSelection(m_ranges[selection], text);
        return true;
    }
    else
    {
        return false;
    }
}

//----------------------------------------------------------------
//! returns the number of selectable blocks in the document.
int TextEditExt::selectionCount() const
{
    return m_ranges.size();
}


//----------------------------------------------------------------
void TextEditExt::setSelection(TextRange range, QString *text)
{
    //qDebug() << te->objectName() << range.fromBlock << range.fromOffset << range.toBlock << range.toOffset;
    range.fromBlock--;  // first Block is 1. Needs to be 0 here.
    range.toBlock--;
    QTextCursor cursor(document());
    cursor.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor, range.fromBlock);
    cursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, range.fromOffset);
    //calculate blocks and characters to move
    int moveBlocks = range.toBlock-range.fromBlock;
    int moveChars;
    if(moveBlocks > 0)
    {
        moveChars = range.toOffset;
        cursor.movePosition(QTextCursor::NextBlock, QTextCursor::KeepAnchor, moveBlocks);
    }
    else
    {
        moveChars = range.toOffset - range.fromOffset;
    }
    cursor.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor, moveChars);
    setTextCursor(cursor);
    if(text != 0)
    {
        *text= cursor.selectedText();
    }
}

//----------------------------------------------------------------
void TextEditExt::contextMenuEvent(QContextMenuEvent *event)
{
    //emit hideTranslation();
}
