#include <QLabel>
#include <QGridLayout>
#include <QDebug>

#include "controlpanel.h"
#include "button.h"

ControlPanel::ControlPanel(QWidget *parent) :
    QWidget(parent)
{
    QPixmap up(":images/button_up.png");
    QPixmap down(":images/button_down.png");

    buttonPlay         = new Button;
    buttonRepeat       = new Button;
    buttonFirst        = new Button;
    buttonPrevious     = new Button;
    buttonNext         = new Button;
    buttonEnd          = new Button;
    m_recCount           = new QLabel;



    buttonPlay->setBackground( up, down);
    buttonRepeat->setBackground( up, down);
    buttonFirst->setBackground( up, down);
    buttonPrevious->setBackground( up, down);
    buttonNext->setBackground( up, down);
    buttonEnd->setBackground( up, down);

    QIcon playIcon(":images/player_play.png");
    //playIcon.addPixmap( QPixmap(":images/player_play.png") , QIcon::Normal, QIcon::On  );
    playIcon.addPixmap( QPixmap(":images/player_pause.png") , QIcon::Normal, QIcon::On  );
    buttonPlay->setIcon(playIcon);
    buttonPlay->setCheckable(true);
    buttonRepeat->setIcon(QIcon(":images/redo.png"));
    buttonRepeat->setCheckable(true);
    buttonFirst->setIcon(QIcon(":images/player_start.png"));
    buttonPrevious->setIcon(QIcon(":images/player_rew.png"));
    buttonNext->setIcon(QIcon(":images/player_fwd.png"));
    buttonEnd->setIcon(QIcon(":images/player_end.png"));

    m_gridLayout = new QGridLayout;
    m_gridLayout->addWidget(buttonPlay,0, 0);
    m_gridLayout->addWidget(buttonRepeat,0, 1);
    m_gridLayout->addWidget(buttonPrevious,1, 0);
    m_gridLayout->addWidget(buttonNext,1, 1);
    m_gridLayout->addWidget(buttonFirst,2, 0);
    m_gridLayout->addWidget(buttonEnd,2, 1);
    m_buttonsLayout = new QVBoxLayout(this);

    m_buttonsLayout->addLayout(m_gridLayout);
    m_buttonsLayout->addStretch();


    QHBoxLayout *recCountLayout = new QHBoxLayout;
    m_recCount->setAlignment(Qt::AlignRight);
    recCountLayout->addWidget(m_recCount);
    //recCountLayout->setSizeConstraint(QLayout::SetNoConstraint);
    m_buttonsLayout->addLayout(recCountLayout);

    //recCount->setText("hello");

    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    setStyleSheet("QWidget { background: gray }");

}

void ControlPanel::setRecordCount(int current, int total)
{
    m_recCount->setText( QString("%1 / %2").arg(current).arg(total) );
    qDebug() << QString("%1 / %2").arg(current).arg(total);
}

void ControlPanel::onPlayChanged(bool play)
{
    bool isChecked = buttonPlay->isChecked();

  /*
     needToggle play checked:
     1          1    0
     1          0    1

*/
    const bool needToggle = play ^ isChecked;
    if(needToggle) {
        buttonPlay->toggle();
    }
}

#include <QStyleOption>
#include <QPainter>
void ControlPanel::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
