#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <QObject>
#include <QTimer>
#include <QPixmap>

#include "tocitem.h"
#include "abstractmediaplayer.h"


class TextEditExt;
class QString;
class QVideoWidget;
class QLabel;
class CenteredWidget;
class ReadLocation;


class Mediator : public QObject
{
    Q_OBJECT
public:
    Mediator(const ReadLocation &readLocation, AbstractMediaPlayer &mediaPlayer);
    void loadChapter(const TocItem &tocItem);
    void setCenteredWidget(CenteredWidget *centeredWidget);
    void setTextEdits(TextEditExt *native, TextEditExt *foreign);

    void loadAudio(const QString &file);

    void enableAudio(bool enabled);
    void enableVideo(bool enabled);
    void enableTextForeign(bool enabled);
    void enableTextNative(bool enabled);

protected:
     bool eventFilter(QObject *obj, QEvent *event) override;

    int id();
public slots:
    void next();
    void previous();
    void goToFirst();
    void goToLast();
    void play(bool playing);
    void repeat(bool startRepeat);
    void stop();
    void pause();  // not sure if pause is needed
    void jumpTo(int record);
private slots:
    void timerHit();

signals:
      void movieMode(bool showMovie);
      void playChanged(bool playing);

private:
    QTimer m_timer;
    QList<AudioRange> m_ranges;
    TextEditExt *m_textEditNative;
    TextEditExt *m_textEditForeign;
    CenteredWidget *m_centeredWidget;
    AbstractMediaPlayer &m_player;
    QVector<QPixmap> m_pictures;

    int m_id;
    int m_currentRec;
    bool m_audioEnabled;
    bool m_videoEnabled;
    bool m_picsEnabled;
    QList<int> m_picIndexes;
    bool m_textForeignEnabled;
    bool m_textNativeEnabled;
    bool m_repeatLoop;
    const ReadLocation &m_readLocation;

    int audioRangeIndex(float time);
};

#endif // MEDIATOR_H
