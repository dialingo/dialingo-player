#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "toctree.h"


namespace Ui {
    class MainWindow;
}

class QTextEdit;
class TextEditExt;
class Button;
class QHBoxLayout;
class QGridLayout;
class QVBoxLayout;
class QLabel;
class QSplitter;
class QVideoWidget;
class ControlPanel;
class Popup;
class QTreeWidgetItem;
class QStackedWidget;
class CenteredWidget;
class DaoRead;
class Mediator;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(DaoRead &daoRead, Mediator &mediator, QVideoWidget *videoWidget);

    ~MainWindow();
public slots:
    void setRecordCount(int current, int total);

private slots:
    void on_actionAbout_triggered();

private slots:
    void on_actionExit_triggered();

private slots:
    void translationRequested(QPoint pos, const QString &word);
    void hideTranslation();
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void movieMode(bool showMovie);
    void onTabWidgetChanged(int index);

private:
    void setupContentWidget();
    Ui::MainWindow *m_ui;
    TocTree m_tocTree;
    Mediator &m_mediator;

    TextEditExt *m_native;
    TextEditExt *m_foreign;

    ControlPanel *controlPanel;
    CenteredWidget *m_centeredWidget;
    QHBoxLayout *m_tabLayout;
    QHBoxLayout *m_textPairLayout;
    QSplitter *m_splitter;
    QWidget *m_textPair;
    Popup *m_popup;
    int m_currentTabIndex;
    DaoRead &m_daoRead;
    QVideoWidget *m_video;
};

#endif // MAINWINDOW_H
