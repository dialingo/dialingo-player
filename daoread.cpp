#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

#include "daoread.h"


//---------------------------------------------------------
DaoRead::DaoRead()
{
}


//---------------------------------------------------------
bool DaoRead::open(QString file)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(file);
    if (db.open())
    {
        //qDebug() <<"db is open: "<< file  << endl;
        return true;
    }
    else
    {
        qDebug() << "opening db failed"<< db.lastError().text().toLatin1().data()  <<  endl;
        return false;
    }
}

//---------------------------------------------------------
void DaoRead::readToc(QList<TocItem> &table)
{
    QSqlQuery query("SELECT  Chapter, ChapterText, Hierarchy, IsFolder, ID FROM toc order by position");
    int fieldChapter = query.record().indexOf("Chapter");
    int fieldChapterText = query.record().indexOf("ChapterText");
    int fieldHierarchy = query.record().indexOf("Hierarchy");
    int fieldIsFolder = query.record().indexOf("IsFolder");
    int fieldId = query.record().indexOf("ID");

    while (query.next())
    {
        TocItem toc;
        toc.chapter = query.value(fieldChapter).toString();
        toc.text = query.value(fieldChapterText).toString();
        toc.hierarchy = query.value(fieldHierarchy).toInt();
        toc.isFolder = query.value(fieldIsFolder).toBool();
        toc.id = query.value(fieldId).toInt();
        table.push_back(toc);
    }
}


//----------------------------------------------------------
void DaoRead::readChapter(int id, TocItem &toc)
{
    QString queryString= QString("SELECT * FROM toc where ID=%1").arg(id);
    QSqlQuery query(queryString);
    const int fieldChapter            = query.record().indexOf("Chapter");
    const int fieldChapterText        = query.record().indexOf("ChapterText");
    const int fieldHierarchy          = query.record().indexOf("Hierarchy");
    const int fieldIsFolder           = query.record().indexOf("IsFolder");
    const int fieldDocForeign         = query.record().indexOf("DocForeign");
    const int fieldDocNative          = query.record().indexOf("DocNative");
    const int fieldAudioFileName      = query.record().indexOf("AudioFileName");
    const int fieldVideoFileName      = query.record().indexOf("VideoFileName");
    const int fieldAudioIndex         = query.record().indexOf("AudioIndex");
    const int fieldTextIndexesForeign = query.record().indexOf("TextIndexesForeign");
    const int fieldTextIndexesNative  = query.record().indexOf("TextIndexesNative");
    const int fieldSentencesForeign   = query.record().indexOf("SentencesForeign");
    const int fieldSentencesNative    = query.record().indexOf("SentencesNative");
    const int fieldID                 = query.record().indexOf("ID");
    const int fieldPictures           = query.record().indexOf("Pictures");
    const int fieldPictureIndexes     = query.record().indexOf("PictureIndexes");


    if (query.next())
    {
        toc.chapter = query.value(fieldChapter).toString();
        toc.text = query.value(fieldChapterText).toString();
        toc.hierarchy = query.value(fieldHierarchy).toInt();
        toc.isFolder = query.value(fieldIsFolder).toBool();
        toc.docForeign = query.value(fieldDocForeign).toByteArray();
        toc.docNative = query.value(fieldDocNative).toByteArray();
        toc.audioFileName = query.value(fieldAudioFileName).toString();
        toc.videoFileName = query.value(fieldVideoFileName).toString();
        toc.ranges = demarshall<QList<AudioRange> >(query.value(fieldAudioIndex).toByteArray()) ;

        toc.textRangesForeign = demarshall<QList<TextRange> >(query.value(fieldTextIndexesForeign).toByteArray()) ;
        toc.textRangesNative = demarshall<QList<TextRange> >(query.value(fieldTextIndexesNative).toByteArray()) ;
        toc.sentencesForeign = demarshall<QStringList>(query.value(fieldSentencesForeign).toByteArray()) ;
        toc.sentencesNative = demarshall<QStringList>(query.value(fieldSentencesNative).toByteArray()) ;
        //QByteArray byteArray = query.value(fieldPictures).toByteArray();
        toc.pictures = demarshall<QList<QByteArray> >(query.value(fieldPictures).toByteArray()) ;
        //QByteArray indexesByteArray = query.value(fieldPictureIndexes).toByteArray();
        toc.pictureIndexes = demarshall<QList<int> >(query.value(fieldPictureIndexes).toByteArray());

        //int size = toc.textRangesForeign.size();  //for debugging
        //QList<TextRange> textsRanges = toc.textRangesForeign;


                //toc.
        bool ok;
        toc.id = query.value(fieldID).toInt(&ok);
        //qDebug() << "loading foreign: " << toc.docForeign.size();
        //table.push_back(toc);
    }

/*

    QString queryStringIndex= QString("select * from audioindex where link=%1 order by posFrom").arg(toc.id);
    QSqlQuery queryIndex(queryStringIndex);
    int fieldFrom = queryIndex.record().indexOf("posFrom");
    int fieldTo = queryIndex.record().indexOf("posTo");
    while(queryIndex.next())
    {
        AudioRange audioRange;
        audioRange.from = queryIndex.value(fieldFrom).toDouble();
        audioRange.to = queryIndex.value(fieldTo).toDouble();
        toc.ranges.push_back(audioRange);
    }
    QString queryStringTextPosition= QString("select * from textindex where link=%1 order by id")
            .arg(toc.id);
    QSqlQuery queryTextPosition(queryStringTextPosition);
    int isNative = queryTextPosition.record().indexOf("isNative");
    int paraFrom = queryTextPosition.record().indexOf("paraFrom");
    int paraTo = queryTextPosition.record().indexOf("paraTo");
    int posFrom = queryTextPosition.record().indexOf("posFrom");
    int posTo = queryTextPosition.record().indexOf("posTo");
    int posParaFrom = queryTextPosition.record().indexOf("posParaFrom");
    int posParaTo = queryTextPosition.record().indexOf("posParaTo");
    while(queryTextPosition.next())
    {
        TextRange textRange;
        textRange.fromBlock = queryTextPosition.value(paraFrom).toInt();
        textRange.toBlock = queryTextPosition.value(paraTo).toInt();
        textRange.fromOffset = queryTextPosition.value(posParaFrom).toInt();
        textRange.toOffset = queryTextPosition.value(posParaTo).toInt();
        textRange.fromTotal = queryTextPosition.value(posFrom).toInt();
        textRange.toTotal = queryTextPosition.value(posTo).toInt();
        bool native = queryTextPosition.value(isNative).toBool();
        if(native)
        {
            toc.textRangesNative.push_back(textRange);
        }else
        {
            toc.textRangesForeign.push_back(textRange);
        }
    }
    */
}


//----------------------------------------------------------
QString DaoRead::translation(const QString &word)
{
    QString queryString= QString("select entry  from dicnormalize,  dicword where  word = '%1' and dicnormalize.base = dicword.base and entry NOT NULL ").arg(word);
    QSqlQuery query(queryString) ;
    if(query.next())
    {
        return query.value(0).toString();
    }
    else
    {
        return "";
    }
}


