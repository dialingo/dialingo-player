
#include <QBitmap>
#include "popup.h"

Popup::Popup(QWidget *parent)
   : QLabel(parent)
{
    QPixmap pixmap(":/images/popup_mask.png");
    setPixmap(pixmap);
    setMask(pixmap.mask());
    const QSize formSize = pixmap.size();
    resize(formSize);
    m_label = new QLabel(this);
    m_label->setAlignment(Qt::AlignTop);
    const int PADDING = 10;
    m_label->setGeometry(PADDING, PADDING, formSize.width() - PADDING, formSize.height() -PADDING );
}


//----------------------------------------------------------
void Popup::setText(const QString &text)
{
    m_label->setText(text);
}


