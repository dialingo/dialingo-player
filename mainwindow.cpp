#include <QDebug>
#include <QFile>
#include <QSplitter>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QLabel>
#include <QBitmap>
#include <QVideoWidget>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "daoread.h"
#include "button.h"
#include "controlpanel.h"
#include "texteditext.h"
#include "popup.h"
#include "centeredwidget.h"
#include "mediator.h"


//---------------------------------------------------------------
MainWindow::MainWindow(DaoRead &daoRead, Mediator &mediator, QVideoWidget *videoWidget)
    :  m_ui(new Ui::MainWindow)
    ,  m_mediator(mediator)
    ,  m_currentTabIndex(-1)
    ,  m_daoRead(daoRead)
    , m_video(videoWidget)
{
    m_ui->setupUi(this);
    setupContentWidget();

    controlPanel = new ControlPanel;

    connect(controlPanel->buttonRepeat, &Button::toggled,  &m_mediator, &Mediator::repeat );
    connect(controlPanel->buttonPlay, &Button::toggled, &m_mediator, &Mediator::play );
    connect(controlPanel->buttonPrevious, &Button::clicked, &m_mediator, &Mediator::previous );
    connect(controlPanel->buttonNext, &Button::clicked, &m_mediator, &Mediator::next );
    connect(controlPanel->buttonFirst, &Button::clicked, &m_mediator, &Mediator::goToFirst );
    connect(controlPanel->buttonEnd, &Button::clicked, &m_mediator, &Mediator::goToLast );
    connect(&m_mediator, &Mediator::movieMode, this, &MainWindow::movieMode );
    connect(&m_mediator, &Mediator::playChanged, controlPanel, &ControlPanel::onPlayChanged);
    connect(m_ui->tabWidget, &QTabWidget::currentChanged, this , &MainWindow::onTabWidgetChanged );


    m_tabLayout->addWidget(controlPanel);
    //ui->treeWidget->header()->hide();
    m_native->setObjectName("native");
    m_foreign->setObjectName("foreign");
    m_native->setReadOnly(true);
    m_foreign->setReadOnly(true);

    m_mediator.setTextEdits(m_native, m_foreign);

    m_tocTree.setTreeWidget(m_ui->treeWidget);
    QList<TocItem> table;
    m_daoRead.readToc(table);
    m_tocTree.loadToc(table);
    m_mediator.setCenteredWidget(m_centeredWidget);

    TocItem toc;
    m_daoRead.readChapter(2, toc);

    m_mediator.loadChapter(toc);
    m_ui->treeWidget->expandAll();
    setWindowTitle("dialingo player");
}

//---------------------------------------------------------------
void MainWindow::setupContentWidget()
{
    m_centeredWidget = new CenteredWidget;
    m_centeredWidget->setVideo(m_video);

    m_textPair = new QWidget;
    m_textPairLayout = new QHBoxLayout(m_textPair);

    m_native = new TextEditExt;
    m_foreign = new TextEditExt;
    m_textPairLayout->addWidget(m_foreign);
    m_textPairLayout->addWidget(m_native);

    m_splitter = new QSplitter;
    m_splitter->setOrientation(Qt::Vertical);
    m_splitter->addWidget(m_centeredWidget);
    m_splitter->addWidget(m_textPair);

    m_tabLayout = new QHBoxLayout(m_ui->tabWork);
    m_tabLayout->addWidget(m_splitter);

    m_popup = new Popup();

    m_popup->setVisible(false);
    connect(m_foreign, &TextEditExt::translationRequested, this, &MainWindow::translationRequested);
    connect(m_foreign, &TextEditExt::hideTranslation, this, &MainWindow::hideTranslation);
}


//---------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete m_ui;
}


//---------------------------------------------------------------
void MainWindow::setRecordCount(int current, int total)
{
    controlPanel->setRecordCount(current, total);
}

//---------------------------------------------------------------
void MainWindow::translationRequested(QPoint pos, const QString &word)
{
    const QPoint ARROW(-12, -85);  // position of of popup arrow
    QPoint popupPos = pos + ARROW ;
    m_popup->move(popupPos);
    m_popup->setVisible(true);
    QString translation = m_daoRead.translation(word);
    QString formattedTrans = QString("<b>%1</b><br>%2").arg(word, translation);
    m_popup->setText(formattedTrans);
}

//---------------------------------------------------------------
void MainWindow::hideTranslation()
{
    m_popup->setVisible(false);
}

//---------------------------------------------------------------
void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column)
    if(item->data(0, TocTree::FOLDER_ROLE).toBool() == true){
        return;
    }
    bool intOk = false;
    int id = item->data(0, Qt::UserRole).toInt(&intOk);
    if(intOk){
        m_ui->tabWidget->setCurrentIndex(1);
        TocItem toc;
        m_daoRead.readChapter(id, toc);
        m_mediator.loadChapter(toc);
    }
}

void MainWindow::movieMode(bool showMovie)
{
    if(showMovie){
        m_centeredWidget->showVideo(true);
    }else{
        m_centeredWidget->showVideo(false);
    }
}

void MainWindow::onTabWidgetChanged(int index)
{
    if(index ==0 && m_currentTabIndex ==1){
        controlPanel->onPlayChanged(false);
        m_mediator.stop();
        QCoreApplication::instance()->removeEventFilter(&m_mediator);
        return;

    }
    if(index == 1) {
        QCoreApplication::instance()->installEventFilter(&m_mediator);
    }
    m_currentTabIndex = index;
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionAbout_triggered()
{
    const char *donations =
R"__(
Content:
This language course was released earlier as a commercial product
"Goldenes Russisch" by Samotec.
Thanks to Rolf Mattern from Samotec for donnating it to the public!

Software:
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
http://www.gnu.org/licenses/
Copyright (c) 2019 Roland Wolf
)__";

    QMessageBox::about(this, "dialingo project", donations );
}
