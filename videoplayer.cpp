#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>
#include <phonon/videowidget.h>
#include "videoplayer.h"


VideoPlayer::VideoPlayer(QObject *parent) :
        QObject(parent)
{
    mediaObject = new Phonon::MediaObject(this);
    connect(mediaObject, SIGNAL(stateChanged(Phonon::State,Phonon::State)), this, SLOT(mediaPlayer_stateChanged(Phonon::State)));
}

//-----------------------------------------------------------------------
void VideoPlayer::setVideoWidget(Phonon::VideoWidget *widget)
{
    videoWidget = widget;
    //mediaObject->setCurrentSource(Phonon::MediaSource( "/home/roland/Documents/kdevelop/American/Audio/612magic1.avi" ));
    Phonon::createPath(mediaObject, videoWidget);
    //mediaObject->play();
}

//-----------------------------------------------------------------------
void VideoPlayer::load(const QString &file)
{
    mediaObject->setCurrentSource(Phonon::MediaSource(file));
    //mediaObject->setCurrentSource(Phonon::MediaSource( "/home/roland/Documents/Musik/If_you_were_a_sailboat.ogg" ));
}

//-----------------------------------------------------------------------
void VideoPlayer::play()
{
    mediaObject->play();
}

//-----------------------------------------------------------------------
void VideoPlayer::pause()
{
    //qDebug() << "pause pressed";
        mediaObject->pause();
}

//-----------------------------------------------------------------------
void VideoPlayer::stop()
{
    //qDebug() << mediaObject;
    mediaObject->stop();
}

//-----------------------------------------------------------------------
void VideoPlayer::seek(long int position)
{
    mediaObject->seek(position);
}

//-----------------------------------------------------------------------
void VideoPlayer::mediaPlayer_stateChanged(Phonon::State newstate)
{
    qDebug() << newstate;
    qDebug() << mediaObject->errorString();
}
