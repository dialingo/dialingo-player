#include <QApplication>
#include <QDebug>
#include <QDir>
#include <memory>

#include "mainwindow.h"
#include "daoread.h"
#include "readlocation.h"
#include "mediator.h"
#include "location.h"
#include "oggplayer.h"
#include "mediaplayer.h"


enum class UseOggVorbis
{
    Yes,
    No
};

enum class UseVideoWidget
{
    Yes,
    No
};


ReadLocation makeReadLocation(int argc, char *argv[], const QDir &appDir)
{
    /*
    find dataDir:
    1. if command has a command line argument use it as dataDir
    2. if there is a directory "media" next to the executable use it as dataDir
    3. if there is a file called dataDir.txt next to the executable take its content as dataDir
    4. use path taken from method dataDir(). This path is ultimately defined in the build script.
    5. --- failed ----
    */
    const QString dataDirFile = appDir.absoluteFilePath("dataDir.txt");
    bool devModeOn = false;
    QString assetPath;
    if(argc < 2 ) {  // no command line argument
        QDir dir = appDir;
        if(dir.cd("media")) {
            assetPath = dir.absolutePath();
        }else if( QFileInfo::exists(dataDirFile) ) {
            QFile file(dataDirFile);
            file.open(QIODevice::ReadOnly);
            QByteArray content = file.readAll();
            assetPath = QString::fromUtf8(content).trimmed();
        }else{
            assetPath = dataDir();
        }
    }else if(argc > 1){ // at least one command line argument
        assetPath = argv[1];
        if(argc >2) {
            devModeOn = (QString(argv[2]) == "--dev");
        }
    }
    return ReadLocation{assetPath, devModeOn};
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Q_INIT_RESOURCE(resources);

    //const UseVideoWidget withVideo = UseVideoWidget::Yes;
    const UseOggVorbis useOgg = UseOggVorbis::Yes;

    app.setApplicationName("dialingoPlayer");
    ReadLocation readLocation = makeReadLocation(argc, argv, QCoreApplication::applicationDirPath());
    if(!QFile(readLocation.targetDb()).exists()) {
        qDebug() << readLocation.targetDb() << " does not exist";
        return 1;
    }

    DaoRead daoRead;
    daoRead.open( readLocation.targetDb() );
    QVideoWidget *videoWidget = new QVideoWidget;  // will be parented later

    std::unique_ptr<AbstractMediaPlayer> abstractMediaPlayer;
    if(useOgg == UseOggVorbis::Yes ) {
        abstractMediaPlayer.reset(new OggPlayer);
    } else {
        abstractMediaPlayer.reset(new MediaPlayer(videoWidget));
    }

    Mediator mediator(readLocation, *abstractMediaPlayer);
    MainWindow mainWindow(daoRead, mediator, videoWidget);
    mainWindow.show();
    app.exec();
}


