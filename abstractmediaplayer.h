#ifndef ABSTRACTMEDIAPLAYER_H
#define ABSTRACTMEDIAPLAYER_H


#include <QString>

enum class MediaplayerState
{
    Stopped,
    Playing,
    Paused,
};

class AbstractMediaPlayer
{
public:
    virtual ~AbstractMediaPlayer() = default;

    virtual void loadBlocking(const QString &file) =0;
    virtual void setPosition(qint64 pos) =0;
    virtual qint64 position() =0;

    virtual void play() = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;
    virtual MediaplayerState state() =0;
};

#endif // ABSTRACTMEDIAPLAYER_H
