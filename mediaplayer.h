#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QObject>
#include <QMediaPlayer>
#include <QVideoWidget>
#include "abstractmediaplayer.h"


class MediaPlayer : public AbstractMediaPlayer
{
public:
    MediaPlayer(QVideoWidget *videoWidget );

    void loadBlocking(const QString &file) override;
    void setPosition(qint64 pos) override ;
    qint64 position() override;
    void play() override;
    void pause() override;
    void stop() override;
    MediaplayerState state() override;


    QMediaPlayer m_mediaPlayer;
};


#endif // MEDIAPLAYER_H
