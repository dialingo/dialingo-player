#ifndef CENTEREDWIDGET_H
#define CENTEREDWIDGET_H


#include <QWidget>

class QLabel;


class CenteredWidget : public QWidget
{
    Q_OBJECT
public:
    CenteredWidget(QWidget *parent=0);
    void setVideo(QWidget *video);
    void showVideo(bool video);
    void setPixmap(const QPixmap &pixmap);

protected:
    void resizeEvent(QResizeEvent * event) override;

private:
    QRect labelPosition(const QSize &size);
    void scaleImage();

    QPixmap m_pixmap;
    QLabel *m_label;
    QWidget *m_video;

};

#endif // CENTEREDWIDGET_H
