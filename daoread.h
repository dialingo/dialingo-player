#ifndef DAOREAD_H
#define DAOREAD_H

#include <QSqlDatabase>
#include <QList>
#include "tocitem.h"


class DaoRead
{
public:
    DaoRead();
    bool open(QString file);   // file is complete path to db file
    bool readToc(const QList<TocItem> &tocRecs);
    void readToc(QList<TocItem> &table);
    void readChapter(int id, TocItem &toc);
    QString translation(const QString &word);

private:
    QSqlDatabase db;
};


template <typename T>
T demarshall(const QByteArray &serializedData)
{
    QDataStream stream(serializedData);
    T outputData;
    stream >> outputData;
    return outputData;
}


#endif // DAOREAD_H
