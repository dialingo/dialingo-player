#include <QAudioFormat>
#include <QAudioDeviceInfo>
#include <QDebug>

#include "oggiodevice.h"
#include "vorbis/vorbisfile.h"

OggIoDevice::OggIoDevice()
{
}

OggIoDevice::~OggIoDevice()
{
    delete m_vf;
}

bool OggIoDevice::openOggVorbis(const QString &fileName)
{
    if(m_file!=nullptr)
    {
        fclose(m_file);
    }
    delete m_vf;
    m_vf = nullptr;
    QByteArray fileNameArray = fileName.toUtf8();
    m_file = fopen(fileNameArray.data(),"rb");
    if(m_file==nullptr)
    {
        qWarning() << "Unable to open file: " << fileName ;
        return false;
    }

    closeOggVorbis();
    m_vf = new  OggVorbis_File;
    if(ov_open_callbacks(m_file, m_vf, nullptr, 0, OV_CALLBACKS_NOCLOSE) < 0)
    {
        qDebug() << QString("file %1 does not seem to be an Ogg bitstream").arg(fileName) ;
        return false;
    } else {
        m_vorbisPointerInitialized = true;
    }

    {
        char **ptr=ov_comment(m_vf,-1)->user_comments;
        vorbis_info *vi=ov_info(m_vf,-1);
        while(*ptr){
            fprintf(stderr,"%s\n",*ptr);
            ++ptr;
        }
        const int channels = vi->channels;
        const long rate = vi->rate;
        qDebug() << QString("Bitstream is %1 channel, %2Hz").arg(channels).arg(rate);
        m_channelCount = channels;
        m_sampleRate = static_cast<int>(rate);
    }
    return true;
}

void OggIoDevice::closeOggVorbis()
{
    if(m_vorbisPointerInitialized){
        ov_clear(m_vf);
        //qWarning() << "close success: " << success;
        m_vorbisPointerInitialized = false;
    }
}

qint64 OggIoDevice::readData(char *data, qint64 maxlen)
{
        qint64 ret=ov_read(m_vf,data, static_cast<int>(maxlen),0,2,1,&m_CurrentSection);
        //qWarning() << "read: " << maxlen <<ret;
        return ret;
}

qint64 OggIoDevice::writeData(const char *data, qint64 len)
{
    Q_UNUSED(data)
    Q_UNUSED(len)
    return 0;
}

int OggIoDevice::channelCount() const
{
    return m_channelCount;
}

int OggIoDevice::sampleRate() const
{
    return m_sampleRate;
}

bool OggIoDevice::isSeekable()
{
    if(m_vf == nullptr) {
        return false;
    } else {
        return ov_seekable(m_vf) != 0;
    }
}

double OggIoDevice::position()
{
    double time = 0.0;
   if(m_vorbisPointerInitialized) {
        time = ov_time_tell(m_vf);
   }
   return time;
}

void  OggIoDevice::seekTime(double time)
{
    QIODevice::seek(0);
    if(m_vorbisPointerInitialized) {
        const int seekResult = ov_time_seek(m_vf,time);
        if(seekResult == 0) {
            return;
        }
        QString message;
        if(seekResult == OV_ENOSEEK) {message= "Bitstream is not seekable.";}
        if(seekResult == OV_EINVAL) {message= "Invalid argument value; possibly called with an OggVorbis_File structure that isn't open. ";}
        if(seekResult == OV_EREAD) {message= "A read from media returned an error";}
        if(seekResult == OV_EFAULT) {message= "Internal logic fault; indicates a bug or heap/stack corruption";}
        if(seekResult == OV_EBADLINK) {message= "Invalid stream section supplied to libvorbisfile, or the requested link is corrupt.";}
        qWarning() << "seek error: " << message;
    }
}

