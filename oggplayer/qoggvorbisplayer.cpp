#include <QDebug>
#include "qoggvorbisplayer.h"


QOggVorbisPlayer::QOggVorbisPlayer(QObject *parent)
    : QObject(parent)
    , m_audioOutput(nullptr)
{
    m_format.setSampleSize(16);
    m_format.setSampleRate(48000);
    m_format.setChannelCount(1);
    m_format.setCodec("audio/pcm");
    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setSampleType(QAudioFormat::SignedInt);
}

bool QOggVorbisPlayer::open(const QString &fileName)
{
    QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
    if (!info.isFormatSupported(m_format)) {
        qWarning() << "Raw audio format is not supported";
        return false;
    }
    m_ioDevice.closeOggVorbis();
    m_ioDevice.openOggVorbis(fileName);
    m_format.setChannelCount(m_ioDevice.channelCount());
    m_format.setSampleRate(m_ioDevice.sampleRate());
    makeAudioOutput();
    return true;
}

void QOggVorbisPlayer::makeAudioOutput()
{
    delete m_audioOutput;
    m_audioOutput  = new QAudioOutput(m_format, this);
    connect(m_audioOutput, SIGNAL(stateChanged(QAudio::State)), this, SLOT(onStateChanged(QAudio::State)));
    m_audioOutput->setBufferSize(4096);
}

void QOggVorbisPlayer::play()
{
    if(m_audioOutput == nullptr) {
        makeAudioOutput();
    }
    if(m_audioOutput != nullptr) {
        m_ioDevice.open(QIODevice::ReadOnly);
        m_audioOutput->start(&m_ioDevice);
        qWarning() << m_audioOutput->error();
    }
}

void QOggVorbisPlayer::stop()
{
    if(m_audioOutput != nullptr) {
        m_audioOutput->stop();
    }
}


void QOggVorbisPlayer::seekTime(double time)
{
    const bool isSeekable = m_ioDevice.isSeekable();
    if(isSeekable == false) {
        qWarning() << "file is not seekable";
        return;
    }
    m_ioDevice.seekTime(time);
}

double QOggVorbisPlayer::time()
{
    return m_ioDevice.position();
}

bool QOggVorbisPlayer::isSeekable()
{
    return m_ioDevice.isSeekable();
}

void QOggVorbisPlayer::onStateChanged(QAudio::State newState)
{
    switch (newState) {
    case QAudio::IdleState:
        qWarning() << "AduioState: " << "IdleState";

        // Finished playing (no more data)
        m_audioOutput->stop();
        // sourceFile.close(); ????
        delete m_audioOutput;
        m_audioOutput = nullptr;
        break;

    case QAudio::StoppedState:
        qWarning() << "AduioState: " << "StoppedState";

        // Stopped for other reasons
        stop();
        seekTime(0);
        if (m_audioOutput->error() != QAudio::NoError) {
            qWarning() << m_audioOutput->error();
            // Error handling
        }
        break;

    case QAudio::ActiveState:
        qWarning() << "AduioState: " << "ActiveState";
        break;
    case QAudio::SuspendedState:
        qWarning() << "AduioState: " << "SuspendedState";
        break;

    case QAudio::InterruptedState:
        qWarning() << "AduioState: " << "InterruptedState";
        break;

    default:
        break;
    }
}
