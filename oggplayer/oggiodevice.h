#ifndef OGGIODEVICE_H
#define OGGIODEVICE_H

#include <QIODevice>


class OggVorbis_File;

class OggIoDevice : public QIODevice
{
public:
    OggIoDevice();
    ~OggIoDevice();
    bool openOggVorbis(const QString &fileName);
    void closeOggVorbis();
    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;

    int channelCount() const;
    int sampleRate() const;
    bool isSeekable();
    double position();
    void seekTime(double time);

private:
    FILE * m_file {nullptr};
    OggVorbis_File *m_vf {nullptr};
    int m_channelCount {0};
    int m_sampleRate {0};
    int m_CurrentSection {0};
    int m_vorbisPointerInitialized {false};
};

#endif // OGGIODEVICE_H
