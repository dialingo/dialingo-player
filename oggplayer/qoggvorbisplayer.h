#ifndef QOGGVORBISPLAYER_H
#define QOGGVORBISPLAYER_H

#include <QObject>
#include <QAudioOutput>

#include "oggiodevice.h"

class QOggVorbisPlayer : public QObject
{
    Q_OBJECT
public:
    explicit QOggVorbisPlayer(QObject *parent = nullptr);

    bool open(const QString &fileName);
    void play();
    void stop();
    void seekTime(double time);
    double time();
    bool isSeekable();

private slots:
    void onStateChanged(QAudio::State newState);
    void makeAudioOutput();

private:
    QAudioOutput *m_audioOutput;
    QAudioFormat m_format;
    OggIoDevice m_ioDevice;
};

#endif // QOGGVORBISPLAYER_H
