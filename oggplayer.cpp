#include "oggplayer.h"

#include <QTime>

OggPlayer::OggPlayer()
{
}

void OggPlayer::loadBlocking(const QString &file)
{
    stop();
     m_oggVorbisPlayer.open(file);

}

void OggPlayer::setPosition(qint64 pos)
{
    const double time = pos / 1000.0;
    m_oggVorbisPlayer.seekTime( time);
}

qint64 OggPlayer::position()
{
    const double elapsed = m_oggVorbisPlayer.time();
    return elapsed * 1000;
}

void OggPlayer::play()
{
    m_oggVorbisPlayer.play();
    m_playerState = MediaplayerState::Playing;
}

void OggPlayer::pause()
{
    stop();
}

void OggPlayer::stop()
{
    m_oggVorbisPlayer.stop();
    m_playerState = MediaplayerState::Stopped;
}

MediaplayerState OggPlayer::state()
{
    return m_playerState;
}

