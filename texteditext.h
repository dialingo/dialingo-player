#ifndef TEXTEDITEXT_H
#define TEXTEDITEXT_H

#include <QTextEdit>
#include <QList>

#include "textrange.h"


class TextEditExt : public QTextEdit
{
    Q_OBJECT
public:
    TextEditExt();
    int selection() const;
    bool setSelection(int selection, QString *text=0);
    void setSelection(TextRange range, QString *text=0);
    int selectionCount() const;
    void setRanges(const QList<TextRange> &range);

signals:
    void recordChanged(int newRecord);
    void translationRequested(QPoint pos, const QString &word);
    void hideTranslation();

private:
    void contextMenuEvent(QContextMenuEvent *event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *e);
    int recordFromCoordinate(int block, int offset) const;

    int m_selection ;
    QList<TextRange> m_ranges;

};

#endif // TEXTEDITEXT_H
