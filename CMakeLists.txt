cmake_minimum_required(VERSION 3.5.0)
project(dialingo_player)


if(UNIX)
    set(DATA_DIR /usr/share/dialingo-russian )
endif()

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Sql REQUIRED)
find_package(Qt5MultimediaWidgets)
find_package(Qt5Multimedia  REQUIRED)

configure_file(location.cpp.in ${CMAKE_CURRENT_BINARY_DIR}/location.cpp )


set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )


set(SRCS
    mainwindow.cpp
    mediator.cpp
    daoread.cpp
    toctree.cpp
    mediaplayer.cpp
    controlpanel.cpp
    button.cpp
    texteditext.cpp
    popup.cpp
    centeredwidget.cpp
    oggplayer.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/location.cpp
) 


if(WIN32)
    set(libs   ${QT_QTMAIN_LIBRARY}
        mingw32 gdi32 comdlg32 oleaut32 imm32 winmm winspool msimg32
        kernel32 user32 shell32 uuid ole32 advapi32 ws2_32 -mwindows
    )
endif(WIN32)


QT5_ADD_RESOURCES(RES  resources.qrc )

set(HDRS
    mainwindow.h
    toctree.h
    mediator.h
    button.h
    controlpanel.h
    texteditext.h
    popup.h
    centeredwidget.h
)

set(uilist  mainwindow.ui )
qt5_wrap_cpp(MOCS  ${HDRS}  )
qt5_wrap_ui(UIS  ${uilist})

add_library(diaclientlib ${SRCS}   ${RES} ${UIS} ${MOCS} )
target_link_libraries(diaclientlib diasharedlib Qt5::Widgets Qt5::Core Qt5::Sql Qt5::MultimediaWidgets)
target_include_directories(diaclientlib
    PRIVATE  ${CMAKE_CURRENT_BINARY_DIR}
    PUBLIC   ${CMAKE_CURRENT_SOURCE_DIR}
    )
set_property(TARGET diaclientlib PROPERTY POSITION_INDEPENDENT_CODE ON)


add_executable(dplayer WIN32 main.cpp )
target_link_libraries(dplayer diaclientlib diasharedlib  qoggvorbisplayer Qt5::Widgets Qt5::Core Qt5::Sql Qt5::MultimediaWidgets)

if(WIN32)
    target_link_libraries(dplayer z zstd)
endif()

if(WIN32)
   install(TARGETS dplayer DESTINATION bin)
elseif(UNIX)   
    install(TARGETS dplayer DESTINATION /usr/local/bin)
endif()

subdirs(clientshared)
subdirs(oggplayer)








