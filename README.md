# dialingo player

Generic language learning application. Presents text, audio, graphics, movie and translation. Avoids the need to lookup unknown words. 
Dialingo player is useless without content.
Content is hosted separately and can be bundled with dialingo player. 