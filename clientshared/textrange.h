#ifndef TEXTRANGE_H
#define TEXTRANGE_H

#include <QString>

struct TextRange
{
TextRange();
qint32 fromBlock;
qint32 toBlock;
qint32 fromOffset;   //offset relative to block
qint32 toOffset;
qint32 fromTotal;    //relativ to start start of document
qint32 toTotal;
QString dump() const;
};

QDataStream &operator<<(QDataStream &stream, const TextRange &range);
QDataStream &operator>>(QDataStream &stream, TextRange &range);



#endif // TEXTRANGE_H
