#include "tocitem.h"


QString TocItem::dump() const
{
    QString serialized(QStringLiteral("%1 %2 loc: %3 text: %4 chapter: %5 page: %6 fileForeign: %7 fileNative: %8 audio: %9  video: %10 index: %11"));
    return serialized
            .arg( isFolder ? QString("folder: ") : ("doc: ")  )
            .arg(hierarchy)
            .arg(location)
            .arg(text)
            .arg(chapter)
            .arg(page)
            .arg(fileNameForeign)
            .arg(fileNameNative)
            .arg(audioFileName)
            .arg(videoFileName)
            .arg(index);

}



QDataStream &operator<<(QDataStream &stream, const AudioRange &range)
{
    return stream << range.from << range.to;
}


QDataStream &operator>>(QDataStream &stream, AudioRange &range)
{
    stream >> range.from;
    stream >> range.to;
    return stream ;
}
