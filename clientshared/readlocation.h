#ifndef READLOCATION_H
#define READLOCATION_H

#include <QString>
#include <QDir>


class ReadLocation
{
public:
    ReadLocation(const QDir &assets, bool devMode);
    QString targetDb() const;
    QString mediaFile(const QString &fileName) const;  //target

private:
    QDir m_projectDir;
    QString m_targetDb;
    bool m_devMode;
};



#endif // READLOCATION_H
