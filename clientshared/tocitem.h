#ifndef TOCITEM_H
#define TOCITEM_H

#include <QString>
#include <QDataStream>
#include <QStringList>
#include <QList>
#include "textrange.h"

struct AudioRange
{
    AudioRange(float from_, float to_) : from(from_), to(to_){}
    AudioRange(){}
    float from;
    float to;
};

QDataStream &operator<<(QDataStream &stream, const AudioRange &range);
QDataStream &operator>>(QDataStream &stream, AudioRange &range);

struct TocItem
{
    int id; //database id
    bool isFolder;
    int hierarchy;
    QString chapter;
    QString text;  //head line = text in chapter selection
    QString page; //link to pdf page
    QString audioFileName;
    QString videoFileName;
    QString location; //name of content folder
    QByteArray docForeign;  //content of file
    QByteArray docNative;
    QByteArray docForeignStripped; // no more tags
    QByteArray docNativeStripped;
    QString fileNameForeign; // name of file
    QString fileNameNative;
    QString index; // audio index file name

    QString dump() const;
    QList<AudioRange> ranges;
    QList<TextRange> textRangesNative;
    QList<TextRange> textRangesForeign;
    QList<QByteArray> pictures;
    QList<int> pictureIndexes;
    QStringList sentencesNative;
    QStringList sentencesForeign;
    QString printOut;  //html for print
};

#endif // TOCITEM_H
