#include <QDataStream>
#include "textrange.h"


TextRange::TextRange()
{
    fromBlock = 0;
    fromOffset = 0;
    fromTotal = 0;
    toBlock = 0;
    toOffset = 0;
    toTotal = 0;
}

//------------------------------------------------------------------
QString TextRange::dump() const
{
    return QString(QStringLiteral("From B: %1 Off: %2 To: B: %3 Off %4")).arg(fromBlock).arg(fromOffset).arg(toBlock).arg(toOffset);
}



QDataStream &operator<<(QDataStream &stream, const TextRange &range)
{
    return stream << range.fromBlock << range.toBlock << range.fromOffset << range.toOffset << range.fromTotal << range.toTotal;
}


QDataStream &operator>>(QDataStream &stream, TextRange &range)
{
    stream >> range.fromBlock;
    stream >> range.toBlock;
    stream >> range.fromOffset;
    stream >> range.toOffset;
    stream >> range.fromTotal;
    stream >> range.toTotal;
    return stream ;
}
