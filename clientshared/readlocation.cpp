#include <QDir>
#include "readlocation.h"


ReadLocation::ReadLocation(const QDir &assets, bool devMode)
    : m_projectDir(assets)
    , m_devMode(devMode)
{
    m_devMode = false;
    m_targetDb = m_projectDir.absoluteFilePath("data.db");
}


QString ReadLocation::targetDb() const
{
    return m_targetDb;
}


QString ReadLocation::mediaFile(const QString &fileName) const
{
    if(m_devMode) {
        QDir projDir = m_projectDir;
        projDir.cdUp();
        return projDir.absoluteFilePath(QString("target_media/")+fileName);
    }
    const QString mediaFileName = m_projectDir.absoluteFilePath(fileName);
    return mediaFileName;
}

