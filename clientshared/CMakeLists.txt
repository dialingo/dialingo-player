 
include_directories( ${CMAKE_CURRENT_BINARY_DIR}/   )
include_directories(${Qt5Widgets_INCLUDE_DIRS})



set(CLIENTHARED_SRCS
    tocitem.cpp
    textrange.cpp
    readlocation.cpp
)

set(CLIENTHARED_HDRS  )

QT5_ADD_RESOURCES(resources   )

qt5_wrap_cpp(CLIENTHARED_MOCS  ${CLIENTHARED_HDRS} )
add_library(diasharedlib ${CLIENTHARED_SRCS}   ${CLIENTHARED_MOCS})
target_include_directories(diasharedlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
set_property(TARGET diasharedlib PROPERTY POSITION_INDEPENDENT_CODE ON)

