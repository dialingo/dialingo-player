#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>

class Button;
class QLabel;
class QGridLayout;
class QVBoxLayout;

class ControlPanel : public QWidget
{
    Q_OBJECT
public:
    explicit ControlPanel(QWidget *parent = 0);
    Button *buttonPlay;
    Button *buttonRepeat;
    Button *buttonFirst;
    Button *buttonPrevious;
    Button *buttonNext;
    Button *buttonEnd;
    void paintEvent(QPaintEvent *);

public slots:
    void setRecordCount(int current, int total);
    void onPlayChanged(bool play); //set back buttonStart

private:
    QGridLayout *m_gridLayout;
    QVBoxLayout *m_buttonsLayout;
    QLabel *m_recCount;
};

#endif // CONTROLPANEL_H
