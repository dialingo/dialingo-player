#ifndef POPUP_H
#define POPUP_H

#include <QLabel>

class Popup : public QLabel
{
    Q_OBJECT
public:
    Popup(QWidget *parent =0);
    void setText(const QString &text);
private:
    QLabel *m_label;
};

#endif // POPUP_H
