#include <QDebug>
#include <QTextEdit>
#include <QDir>
#include <QLabel>
#include <QKeyEvent>

#include "mediator.h"
#include "daoread.h"
#include "abstractmediaplayer.h"
#include "tocitem.h"
#include "readlocation.h"
#include "texteditext.h"
#include "centeredwidget.h"



//-------------------------------------------------------------
Mediator::Mediator(const ReadLocation &readLocation, AbstractMediaPlayer &mediaPlayer)
    : m_textEditNative{nullptr}
    , m_textEditForeign{nullptr}
    , m_centeredWidget{nullptr}
    , m_player(mediaPlayer)
    , m_repeatLoop{false}
    , m_readLocation(readLocation)
{
    m_id = -1;
    m_currentRec = 0;
    m_audioEnabled = true;
    m_videoEnabled= true;
    m_picsEnabled = false;
    m_textForeignEnabled= true;
    m_textNativeEnabled= true;
    m_timer.setInterval(200);
    connect(&m_timer, &QTimer::timeout, this, &Mediator::timerHit);
}

//-------------------------------------------------------------
void Mediator::loadChapter(const TocItem &tocItem)
{
    if(tocItem.pictures.size() > 0 && tocItem.videoFileName.isEmpty()){
        m_picsEnabled = true;
        m_videoEnabled = false;

    }
    m_pictures.clear();
    for(const auto &pic : tocItem.pictures){
        QImage image = QImage::fromData(pic);
        QPixmap pixmap = QPixmap::fromImage(image);
        m_pictures.append(pixmap);
    }
    QString docNative = QString::fromUtf8( tocItem.docNative.constData() );
    m_textEditNative->setHtml(docNative);
    QString docForeign = QString::fromUtf8( tocItem.docForeign.constData() );
    m_textEditForeign->setHtml(docForeign);
    m_ranges = tocItem.ranges;

    if(m_audioEnabled && (tocItem.audioFileName != "") )
    {
        const QString audioFileName = m_readLocation.mediaFile(tocItem.audioFileName);
        m_player.loadBlocking(audioFileName);
    }
    if(m_videoEnabled  && (tocItem.videoFileName != "") )
    {
        const QString videoFileName = m_readLocation.mediaFile((tocItem.videoFileName));
        m_player.loadBlocking(videoFileName);
        emit movieMode(true);
    }else{
        emit movieMode(false);
    }
    //qDebug() << "ranges size: " << tocItem.textRangesForeign.size() << tocItem.textRangesNative.size();
    m_textEditForeign->setRanges( tocItem.textRangesForeign);
    m_textEditNative->setRanges( tocItem.textRangesNative);
    if(tocItem.textRangesForeign.size() != tocItem.textRangesNative.size()){
        qDebug("warning: tocItem.textRangesForeign.size() != tocItem.textRangesNative.size()");
    }
    m_picIndexes = tocItem.pictureIndexes;
    jumpTo(0);
}

//-------------------------------------------------------------
void Mediator::setTextEdits(TextEditExt *native, TextEditExt *foreign)
{
    m_textEditNative = native;
    m_textEditForeign = foreign;
    connect(native, &TextEditExt::recordChanged, this, &Mediator::jumpTo);
    connect(foreign, &TextEditExt::recordChanged, this, &Mediator::jumpTo);
}

//-------------------------------------------------------------
void Mediator::jumpTo(int record)
{
    m_textEditNative->setSelection(record);
    m_textEditForeign->setSelection(record);
    m_currentRec = record;

    if(record < m_ranges.size() && record >= 0 )
    {
        if(m_audioEnabled || m_videoEnabled)
        {
            qint64 position = static_cast<qint64>(m_ranges[record].from * 1000);
            m_player.setPosition(position);
        }
        if(m_picsEnabled){
            if( record  < m_picIndexes.size() ){
                const int picIndex = m_picIndexes.at(record);
                QPixmap pixmap = m_pictures.at(picIndex);
                m_centeredWidget->setPixmap(pixmap);
            }
        }
    }
}

//-------------------------------------------------------------
void Mediator::next()
{
    m_repeatLoop = false;
    int totalRec = m_textEditNative->selectionCount();
    if(m_currentRec > totalRec -2 ) //
    {
        return;
    }
    m_currentRec++;
    jumpTo(m_currentRec);
}

//-------------------------------------------------------------
void Mediator::previous()
{
    m_repeatLoop = false;
    if(m_currentRec < 1)
    {
        return;
    }
    m_currentRec--;
    jumpTo(m_currentRec);
}

//-------------------------------------------------------------
void Mediator::goToFirst()  //=go to start (vs. play)
{
    m_repeatLoop = false;
    m_currentRec = 0;
    jumpTo(m_currentRec);
}

//-------------------------------------------------------------
void Mediator::goToLast()
{
    m_repeatLoop = false;
    m_currentRec = m_textEditNative->selectionCount()-1;
    jumpTo(m_currentRec);
}

//-------------------------------------------------------------
void Mediator::play(bool playing)
{
    m_repeatLoop = false;
    if(playing) {
        m_player.play();
        m_timer.start();
    } else {
        m_player.pause();
        m_timer.stop();
    }
}

void Mediator::repeat(bool startRepeat)
{
    if(startRepeat){
        m_repeatLoop = true;
        m_timer.stop();
        jumpTo(m_currentRec);
        m_timer.start();
        m_player.play();
    } else {
        m_player.pause();
        m_timer.stop();
        m_repeatLoop = false;
    }
}

//-------------------------------------------------------------
void Mediator::pause()
{
    m_player.pause();
}

//-------------------------------------------------------------
void Mediator::stop()
{
    m_repeatLoop = false;
    m_player.stop();
    m_timer.stop();
}


void Mediator::setCenteredWidget(CenteredWidget *centeredWidget)
{
    m_centeredWidget = centeredWidget;
}

//-------------------------------------------------------------
void Mediator::enableAudio(bool enabled)
{
    m_audioEnabled = enabled;
}

//-------------------------------------------------------------
void Mediator::enableVideo(bool enabled)
{
    m_videoEnabled = enabled;
}

//-------------------------------------------------------------
void Mediator::enableTextForeign(bool enabled)
{
    m_textForeignEnabled = enabled;
}

//-------------------------------------------------------------
void Mediator::enableTextNative(bool enabled)
{
    m_textNativeEnabled = enabled;
}

bool Mediator::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        const int keyNr = keyEvent->key();
        if( keyNr == Qt::Key_Down) {
            next();
        } else if(keyNr == Qt::Key_Up) {
            previous();
        } else if(keyNr == Qt::Key_PageUp) {
            goToFirst();
        } else if(keyNr == Qt::Key_PageDown) {
            goToLast();
        }   else if(keyNr == Qt::Key_Space) {
            if(m_player.state() == MediaplayerState::Stopped || m_player.state() == MediaplayerState::Paused) {
                play(true);
                emit playChanged(true);
            } else {
                play(false);
                emit playChanged(false);
            }

        } else {
            return QObject::eventFilter(obj, event);
        }

        return true;
    }
    // standard event processing
    return QObject::eventFilter(obj, event);

}

int Mediator::id()
{
    return m_id;
}

//-------------------------------------------------------------
void Mediator::timerHit()
{
    const float timerPosition = m_player.position() / 1000.0f;
    const int newRecord = audioRangeIndex( timerPosition) ;
    if(newRecord == -1) {
        stop();
        emit playChanged(false);
        return;
    }
    if (m_player.state() == MediaplayerState::Stopped)
    {
        stop();
        emit playChanged(false);

        return;
    }

    if (newRecord != m_currentRec && newRecord >= 0)
    {
        if(m_repeatLoop){
            m_player.pause();
            jumpTo(m_currentRec);
            m_player.play();
            return;
        }
        m_currentRec++;
        m_textEditNative->setSelection(newRecord);
        m_textEditForeign->setSelection(newRecord);
        if(m_picsEnabled){
            if( newRecord  < m_picIndexes.size()  ){
                QPixmap pixmap = m_pictures.at(m_picIndexes.at(newRecord));
                m_centeredWidget->setPixmap(pixmap);
            }
        }
    }
}

//-------------------------------------------------------------
/// @arg time current time
/// @return returns -1 if current time is not within a from/to pair. Otherwhise returns the index of from/to pair
int Mediator::audioRangeIndex(float time)
{
    if((m_ranges.size() > 0) && (time > m_ranges.last().to)){
        return -1;
    }
    auto beginIter = m_ranges.rbegin();
    auto endIter = m_ranges.rend();

    auto found = std::find_if(beginIter,endIter,
                              [time](const AudioRange &range){ return range.from < time; }
    );
    if(found == endIter) {
        return 0;
    }
    const int selectedRange =  m_ranges.size() - (found - beginIter) -1;
    return selectedRange;
}
