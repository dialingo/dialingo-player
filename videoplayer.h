#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QObject>
#include <phonon/mediaobject.h>

namespace Phonon
{
   class MediaObject;
   class VideoWidget;
}

class VideoPlayer : public QObject
{
    Q_OBJECT
public:
    explicit VideoPlayer(QObject *parent = 0);
    void load(const QString &file);
    void play();
    void stop();
    void pause();
    void seek(long int position);
    void setVideoWidget(Phonon::VideoWidget *widget);

private:
    Phonon::MediaObject *mediaObject;
    Phonon::VideoWidget *videoWidget;
    Phonon::Path path;
private slots:
    void mediaPlayer_stateChanged(Phonon::State newstate);
};

#endif // VIDEOPLAYER_H
