#ifndef OGGPLAYER_H
#define OGGPLAYER_H

#include <QObject>
#include <QTimer>
#include "abstractmediaplayer.h"
#include "oggplayer/qoggvorbisplayer.h"

class OggPlayer : public AbstractMediaPlayer
{
    //Q_OBJECT
public:
    explicit OggPlayer();
    void loadBlocking(const QString &file) override;
    void setPosition(qint64 pos) override ;
    qint64 position() override;
    void play() override;
    void pause() override;
    void stop() override;
    MediaplayerState state() override;

//private slots:
//    void timerHit();

private:
    QOggVorbisPlayer m_oggVorbisPlayer;
    //QTimer m_timer;
    //bool m_playing = false;
    MediaplayerState m_playerState;

};

#endif // OGGPLAYER_H
