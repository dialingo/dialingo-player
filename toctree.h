#ifndef TOCTREE_H
#define TOCTREE_H

#include <QObject>
#include <QList>

#include "tocitem.h"

class QTreeWidget;

class TocTree : public QObject
{
    Q_OBJECT
public:
    explicit TocTree(QObject *parent = 0);
    void setTreeWidget(QTreeWidget *treeWidget);
    void loadToc(const QList<TocItem> &table);

    static int ID_ROLE;
    static int FOLDER_ROLE;

private:
    QTreeWidget *m_treeWidget;

};

#endif // TOCTREE_H
