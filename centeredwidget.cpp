#include <QLabel>
#include <QDebug>

#include "centeredwidget.h"


CenteredWidget::CenteredWidget(QWidget *parent)
    : QWidget(parent)
{
    m_label = new QLabel(this);
    m_label->setScaledContents(true);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //setStyleSheet("background-color:red;");
}


void CenteredWidget::setVideo(QWidget *video)
{
    video->setParent(this);
    m_video = video;
    m_video->setGeometry(0,0, width(), height());
}

void CenteredWidget::showVideo(bool video)
{
    m_video->setVisible(video);
    m_label->setVisible(!video);
}



void CenteredWidget::setPixmap(const QPixmap &pixmap)
{
    m_pixmap = pixmap;
    m_label->setPixmap(pixmap);
    scaleImage();
}

void CenteredWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    scaleImage();
}

QRect CenteredWidget::labelPosition(const QSize &size)
{
    QPoint pos;
    const int labelWidth = size.width();
    const int labelHeight = size.height();

    const int excessWidth = width() - labelWidth;
    const int excessHeight = height() - labelHeight;
    if(excessWidth >0 ){
       pos.setX(excessWidth / 2);
    }
    if(excessHeight >0 ){
       pos.setY(excessHeight / 2);
    }
    const QRect rect(pos, size );
    return rect;
}

void CenteredWidget::scaleImage()
{
    const int horizontalScale = width() / m_pixmap.width();
    const int verticalScale = height() / m_pixmap.height();
    const int maxScale = std::min(horizontalScale, verticalScale);
    const int scale = std::max(1, maxScale);

    const QSize scaledSize = m_pixmap.size() * scale;
    const QRect rect = labelPosition(scaledSize);
    m_label->setGeometry(rect);
    m_video->setGeometry(0,0, width(), height());
}

