#include <QTreeWidget>
#include <QDebug>

#include "toctree.h"

int TocTree::ID_ROLE = Qt::UserRole;
int TocTree::FOLDER_ROLE = Qt::UserRole +1;


//----------------------------------------------------------
TocTree::TocTree(QObject *parent) :
        QObject(parent)
{
}

//----------------------------------------------------------
void TocTree::setTreeWidget(QTreeWidget *treeWidget)
{
    m_treeWidget = treeWidget;
}

//----------------------------------------------------------
void TocTree::loadToc(const QList<TocItem> &tocItems)
{
    m_treeWidget->setColumnCount(1);
    QIcon iconDocument(":/images/doc.png");
    QIcon iconFolder(QString(":/images/folder.png"));
    QList<QTreeWidgetItem *> hierarchyTrack;
    for(int i=0; i< tocItems.size(); i++)
    {
        const TocItem &tocItem = tocItems.at(i);
        if(tocItem.hierarchy ==0 )
        {
            QTreeWidgetItem *item = new QTreeWidgetItem( m_treeWidget , QStringList(QString(tocItem.text ) ) );
            item->setIcon(0, tocItem.isFolder?  iconFolder : iconDocument);
            item->setData(0, ID_ROLE, tocItem.id);
            item->setData(0, FOLDER_ROLE, tocItem.isFolder);

            if(hierarchyTrack.size() >0)
            {
                hierarchyTrack[0] = item;
            }
            else
            {
                hierarchyTrack.append(item);
            }
            m_treeWidget->insertTopLevelItem(i, item);
        }
        else
        {
            QTreeWidgetItem *item = new QTreeWidgetItem( QStringList(QString(tocItem.text ) ) );
            item->setIcon(0, tocItem.isFolder?  iconFolder : iconDocument);
            item->setData(0, ID_ROLE, tocItem.id);
            item->setData(0, FOLDER_ROLE, tocItem.isFolder);

            int hierarchy = tocItem.hierarchy;
            if(hierarchyTrack.size() >  hierarchy )
            {
                hierarchyTrack[hierarchy-1]->addChild( item );
            }
            else if (hierarchyTrack.size() ==  hierarchy)
            {
                QTreeWidgetItem *appendItemHere = hierarchyTrack.last();
                appendItemHere->addChild((item));
                hierarchyTrack.append(item);
            }
            else
            {
                qDebug() << "error in toc data";
            }
            m_treeWidget->insertTopLevelItem(i, item);
        }
    }
}

